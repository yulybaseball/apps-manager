#!/bin/env python

"""
Script to manage the applications running on a server.

It can stop, start, restart, clear cache and check status of a given 
application, always by reading from apps.json file.

These options are defined in the configuration file:
 - mandatory_up: Defines if it's mandatory for the app to be running. Value 
    can be specified using true or 1 for mandatory, and false or 0 (zero) for 
    non-mandatory. If this key/value option is not on the config file,
    no assumption will be made on this matter.
 - start: Complete path to the script file to be executed to start up the app.
    If this key/value is not in the config file, this script won't be able to 
    start the app.
 - stop: Complete path to the script file to be executed to shut down the app.
    If this key/value is not in the config file, this script will attempt to 
    kill the app using its name.
- status: Complete path to the script to be executed in order to get the 
    status of the app. If this key/value is not specified, this script will 
    determine the status of the app by searching its name between the running 
    processes.
- clear_cache: Complete path to the script to be executed to clear cache. If 
    this key/value is not specified, this script won't be able to clear cache 
    for the app.

Bash commands can be specified as actions, like below:
    cd /home/weblogic/path; ./start.sh >> app_test.log &;

Usage:

./apps.py <action> <app> [-e <email_address>] [-k <attempts>] [-m <mandatory>]

 - action: can be one of these (case insensitive):
    start: start specified app
    stop: stop specified app
    status: get status of the specified app
    restart: restart specified app
    ccrestart: stop, clear cache and start specified app
    resume: get a resume (name and mandatory) of the specified app. This 
        option is useful when app == all
    switch: switch the mandatory value of the specified app

 - app: can be the name of any app (case sensitive), or the 
        word 'all' (without the single quotes, and case insensitive)

Other options:

 -e <email_address>: This script will attempt to send an email to 
    email_address when the action has been sent, containing the result of the 
    action, in cases like 'mandatory', 'resume', 'status', 'switch'.

 -k <attempts>: Useful only for actions 'stop', 'restart' and 'ccrestart'. 
    It defines the amount of attempts (integer number between 1 and 50) 
    this script will check the specified app before killing it. Normally, 
    this script doesn't wait nor check for status of the app after stopping it 
    if this parameter is not provided. Checking attempts are made every 20 
    seconds.

 -m <mandatory>: Useful only for action 'resume' when specified with app 
    'all'. It tells this script to get the resume of those apps which 
    'mandatory_up' is the one specified on this parameter. Values passed in 
    can be 1 or 0 (zero).

Please note thise options should be specified in lowercase.

"""

import datetime
import getopt
import json
import logging
from os import path, system
import sys

import subprocess

logger = None
this_process_id = None

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
CONF_FILE_NAME = path.join(EXEC_DIR, 'apps.json')
LOG_FILE_NAME = path.join(EXEC_DIR, 'apps.log')

ACTIONS = ['start', 'stop', 'status', 'restart', 'ccrestart', 'resume', 'switch']
OPTIONS = "e:k:m:"
MIN_ATTEMPTS = 1
MAX_ATTEMPTS = 50

# alternative commands
STATUS_CMD = "ps -ef | grep '{0} ' | grep -v grep | grep -v apps.py"

# email
SUBJECT = "apps.py - {action} {app}"
BODY = ("This email was requested as part of executing 'apps.py' on server " + 
        "{server}\nCommand executed: {command}\nResult:\n\t{result}")
SENDER = "ProductionSystemsSupport@tracfone.com"

def _define_logger():
    """Create logger and add a file handler and a formatter to it."""
    global logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    if not logger.handlers:
        proc_id_fmt = {'process_id': this_process_id}
        handler_info = logging.FileHandler(LOG_FILE_NAME)
        fomatter = '%(asctime)s - %(process_id)s - %(levelname)s - %(message)s'
        handler_info.setFormatter(logging.Formatter(fmt=fomatter))
        logger.addHandler(handler_info)
        logger = logging.LoggerAdapter(logger, proc_id_fmt)

def _set_process_id():
    """Set this_process_id to a 'unique' value: a number made up today's 
    date and current time.
    This will make easier to identify log lines for the same call/process.
    """
    global this_process_id
    this_process_id = str(datetime.datetime.now()).replace('-', '').\
                                                   replace(':', '').\
                                                   replace(' ', '').\
                                                   replace('.', '')

def _quit_script(errmsg):
    """Print errmsg in the shell and log, and exit this script."""
    print(errmsg)
    logger.error(errmsg)
    exit()

def _check_attempts_arg(att_arg):
    """Check att_arg is an integer in the range defined by MIN_ATTEMPTS and 
    MAX_ATTEMPS. Return True if it is; False otherwise.
    """
    try:
        int_att_arg = int(att_arg)
        return int_att_arg >= MIN_ATTEMPTS and int_att_arg <= MAX_ATTEMPTS
    except:
        return False

def _check_params():
    """Check params passed to this script. Whenever the checking fails, 
    print the error to the log file and exit.
    Return a tuple containing the list of arguments and the list of 
    tuples of the form (option, value).
    """
    fullparams = sys.argv
    params = fullparams[1:]
    logger.info("I was called this way: {0}".format(" ".join(fullparams)))
    if len(params) < 2:
        _quit_script("I require at least 2 parameters.")
    arguments = params[:2]
    if arguments[0].lower() not in ACTIONS:
        _quit_script("Action '{0}' is not recognized.".format(arguments[0]))
    try:
        options, values = getopt.getopt(params[2:], OPTIONS)
        if values:
            _quit_script("Argument not recognized.")
        for opt, val in options:
            if opt == '-k' and not _check_attempts_arg(val):
                _quit_script("'-k' (attempts) value is not valid.")
            if opt == '-m' and val not in ['0', '1']:
                _quit_script("'-m' (mandatory) value is not valid.")
        # logger.info("Arguments: {0}".format(arguments))
        # logger.info("Options: {0}".format(options))
        # logger.info("Values: {0}".format(values))
        return (arguments, options)
    except getopt.error as err:
        _quit_script(str(err))

def _get_app_conf(app):
    try:
        with open(CONF_FILE_NAME) as app_conf:
            apc = json.load(app_conf)
            return apc[app] if app.lower() != 'all' else apc
    except Exception as e:
        _quit_script(("Loading conf for app {0}: {1}. Does this app exist?").\
                    format(app, str(e)))

def _execute_command(cmd, sync=False):
    if sync:
        p = subprocess.Popen(str(cmd), stdin=subprocess.PIPE,
                    shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return p.communicate()
    return system(cmd)

def _get_pid(processes):
    """Return the PID (or PIDs) from processes."""
    pids = []
    try:
        for process in processes.split('\n'):
            if process.strip():
                pids.append(process.split(' ')[1])
    except Exception as e:
        logger.error("Searching PID: {0}".format(str(e)))
    return pids

def _create_email(email_file_name, subject, body, sender, dest):
    """Create email file. Return True if created; False otherwise."""
    ctstr = "text/plain; charset=UTF-8"
    content = (("From: {0}\n" + 
               "Date: 666\n" + 
               "To: {1}\n" + 
               "Subject: {2}\n" +
                "Content-Type: {3}\n" +  
               "\n{4}\n").format(sender, dest, subject, ctstr, body))
    try:
        with open(email_file_name, 'w+') as email_file:
            email_file.write(content)
        return True
    except BaseException:
        return False

def _send_email(app, dest, message, action, options):
    currdatetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    email_file_name = '{0}_{1}_{2}.em'.format(app, action, currdatetime)
    subject = SUBJECT.format(app=app, action=action)
    opts = ""
    for opt in options:
        opts += "{0} {1} ".format(opt[0], opt[1])
    command = "{0} {1} {2}".format(action, app, opts)
    server, dummy = _execute_command("hostname", sync=True)
    body = BODY.format(server=server, command=command, result=message)
    if _create_email(email_file_name, subject, body, SENDER, dest):
        cmd = "mailx -t < {0}".format(email_file_name)
        try:
            _execute_command(cmd)
            return "Email was requested to be sent to {0}".format(dest)
        except BaseException as e:
            return str(e)
        finally:
            cmd = "rm -f -- {0}".format(email_file_name)
            _execute_command(cmd)
    else:
        return "Email could not be sent: email file doesn't exist."

def _app_resume(app, app_conf, mandatory=None):
    """Get and print the mandatory_up value, only if value matches mandatory 
    parameter value. Print always when mandatory is None.
    """
    mand = app_conf['mandatory_up']
    msg = "{0} -> ".format(app)
    if mand not in [0, 1, True, False]:
        msg += "WARNING: not a valid value."
    else:
        val = bool(mand)
        bool_mand = bool(int(mandatory)) if mandatory != None else mandatory
        if (bool_mand and val) or (bool_mand == False and not val) or \
            (mandatory == None):
            msg += str(val)
        else:
            return ""
    logger.info(msg)
    print(msg)
    return msg

def _resume(app, app_conf, options):
    """Return a resume for the app."""
    logger.info("Getting a resume of {0}.".format(app))
    status = []
    mand = None
    dest = False
    for option in options:
        if option[0] == '-m':
            mand = option[1]
        elif option[0] == '-e':
            dest = option[1]
    if app.lower() == 'all':
        for ap in app_conf:
            status.append(_app_resume(ap, app_conf[ap], mand))
    else:
        status.append(_app_resume(app, app_conf, mand))
    if dest:
        messages = "\n\t".join(status)
        sem = _send_email(app, dest, messages, 'resume', options)
        logger.info(sem)

def _app_status(app, app_conf):
    """Check status of app. Return a dictionary contaning the app name as 
    the key, with 2 values: running (True if it is running, even with more 
    than 1 PID runnning, False id no PID was found, or None, if it couldn't 
    be determined the status, mostly when the status is checked using the 
    key 'status' on the app_conf), and msg (the text specifying the status of 
    the app)."""
    status = {'running': None, 'msg': ""}
    logger.info("Checking status of {0}.".format(app))
    by_pid = False
    if 'status' in app_conf and app_conf['status']:
        command = app_conf['status']
    else:
        command = STATUS_CMD.format(app)
        by_pid = True
    stdout, stderr = _execute_command(command, sync=True)
    if stderr.strip() and not stdout:
        msg = ("Executing command {0} for getting status of " + 
               "app {1}: {2}").format(command, app, stderr)
        logger.error(msg)
        msg = "ERROR: " + msg
        print(msg)
        status['msg'] = msg
        return status
    msg = stdout.strip()
    if by_pid:
        pids = _get_pid(stdout)
        if pids:
            if len(pids) > 1:
                msg = ("{0} might be running more than once. " + 
                    "These PIDs reference processes with that name: {1}").\
                        format(app, ", ".join(pid for pid in pids if pid))
                logger.warning(msg)
                msg = "WARNING! " + msg
            else:
                msg = "{0} is running with PID {1}.".format(app, pids[0])
                logger.info(msg)
            print(msg)
            status['msg'] = msg
            status['running'] = True
            return status
        else:
            msg = "{0} is not running.".format(app)
            status['msg'] = msg
            status['running'] = False
    print(msg)
    logger.info(msg)
    status['msg'] = msg
    return status

def _status(app, app_conf, options):
    """Check the status of a single app, or all apps (in case 'all' is padded 
    as parameter). Send email if the option was specified.
    """
    status = {}
    # check status for every single app
    if app.lower() == 'all':
        for ap in app_conf:
            status[ap] = _app_status(ap, app_conf[ap])
    else:
        status[app] = _app_status(app, app_conf)
    # send email if option is specified
    for option in options:
        if option[0] == '-e':
            dest = option[1]
            messages = ""
            for app in status:
                messages += (status[app]['msg'] + '\n\t')
            sem = _send_email(app, dest, messages, 'status', options)
            logger.info(sem)

def _app_start(app, app_conf, mand=None):
    ap_mand = app_conf['mandatory_up']
    if mand == None or (bool(int(mand)) and ap_mand) or \
                    (not bool(int(mand)) and not ap_mand):
        app_status = _app_status(app, app_conf)
        if not app_status['running']:
            msg = "Trying to start app {0}.".format(app)
            logger.info(msg)
            print(msg)
            cmd = app_conf['start']
            _execute_command(cmd)
            return "Command was sent to start app {0}".format(app)
        return "App {0} was already running.".format(app)
    mandtxt = "is not" if not ap_mand else "is"
    return ("{0} {1} mandatory to start and this option was passsed in: {2}").\
        format(app, mandtxt, "-m {0}".format(mand))

def _start(app, app_conf, options):
    """Start app."""
    dest = False
    mand = None
    messages = []
    for option in options:
        if option[0] == '-e':
            dest = option[1]
        elif option[0] == '-m':
            mand = option[1]
    if app.lower() == 'all':
        for ap in app:
            msg = _app_start(ap, app_conf[ap], mand)
            print(msg)
            messages.append(msg)
            logger.info(msg)
    else:
        msg = _app_start(app, app_conf, mand)
        print(msg)
        messages.append(msg)
        logger.info(msg)
    if dest:
        message = "\n\t".join(messages)
        sem = _send_email(app, dest, message, 'start', options)
        logger.info(sem)

def _stop(app, app_conf, options):
    """Stop app. Use action defined in conf file, or kill if stop was not 
    defined.
    """
    logger.info("_stop...{0} {1} {2}".format(app, app_conf, options))

def _ccrestart(app, app_conf, options):
    """Stop app. Use action defined in conf file, or kill if stop was not 
    defined.
    """
    logger.info("_ccrestart...{0} {1} {2}".format(app, app_conf, options))

def _restart(app, app_conf, options):
    """Stop app. Use action defined in conf file, or kill if stop was not 
    defined.
    """
    logger.info("_restart...{0} {1} {2}".format(app, app_conf, options))

def _switch(app, app_conf, options):
    """Stop app. Use action defined in conf file, or kill if stop was not 
    defined.
    """
    logger.info("_switch...{0} {1} {2}".format(app, app_conf, options))

def _main():
    _set_process_id()
    _define_logger()
    arguments, options = _check_params()
    action = arguments[0]
    app = arguments[1]
    app_conf = _get_app_conf(app)
    try:
        func = "_{0}('{1}', {2}, {3})".format(action, app, app_conf, options)
        eval(func)
    except Exception as e:
        _quit_script(str(e))
    exit(0)

# entry point
_main()
